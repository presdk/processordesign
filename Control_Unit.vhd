library ieee;
use IEEE.std_logic_1164.all;
use work.instruction_map.all;

entity Control_Unit is
port (clk		: in std_logic;
		reset 	: in std_logic;
		Z			: in std_logic;
      AM			: in std_logic_vector(1 downto 0);
		opcode	: in std_logic_vector(5 downto 0);
		start 	: out std_logic;
		MemRead, MemWrite, IRwrite, PCwrite, IROPwrite, RegRead, RegWrite, DataMemWrSrc, SSOPwrite, Zwrite : out std_logic := '0';
		ALUSrcA, ALUSrcB, PCsrc, ALUOP, MemToReg, DataMemSrc : out std_logic_vector(1 downto 0) := "00";
		logical_one : out std_logic_vector(15 downto 0) := "0000000000000001";
		instruction_o : out std_logic_vector(7 downto 0);
		state : out integer
);
end Control_Unit;

architecture arch of Control_Unit is

type state_type is (t0,t1,t2,t3,t4);  --type of state machine.
signal current_s, next_s: state_type;  --current and next state declaration.
signal instruction : std_logic_vector(7 downto 0);

begin

instruction <= opcode & AM;
instruction_o <= opcode & AM;
logical_one <= "0000000000000001";
start <= reset;

process (clk, reset)
begin
	if (reset = '1') then
		current_s <= t0;  --default state on reset.
	elsif (clk'event and clk = '1') then
		current_s <= next_s;   --state change.
	end if;
end process;

--state machine process.
process (current_s)
begin
MemRead <= '0';
MemWrite <= '0';
IRwrite <= '1';
PCwrite <= '1';
IROPwrite <= '0';
RegRead <= '0';
RegWrite <= '0';
SSOPwrite <= '0';
Zwrite <= '0';
ALUSrcA <= "00";
ALUSrcB <= "00";
PCsrc <= "10";
ALUOP <= "00";
MemToReg <= "00";
DataMemSrc <= "00";
DataMemWrSrc <= '0';

	case instruction is
		when ADD_Reg --TODO
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
  case current_s is
		when t0 =>	--Compute next PC and Fetch instruction
			PCwrite <= '1';
			PCsrc <= "10";
			
			state <= 0;
			next_s <= t1;
		when t1 =>	--Load IR
			IRwrite <= '1';
			
			state <= 1;
			next_s <= t2;
		when t2 => --Fetch registers 
			if(instruction = ADD_Reg or
			instruction = AND_Reg or
			instruction = LDR_Reg or
			instruction = STR_Reg or
			instruction = SSOP or
			instruction = JMP_Reg) then
				RegRead <= '1';
			elsif(instruction = ADD_Op or --Fetch Registers and Operands
			instruction = AND_Op or
			instruction = SUB_OP or
			instruction = SUBV_Op or
			instruction = LDR_Op or
			instruction = STR_Op or
			instruction = STR_OpAddress or
			instruction = SZ or
			instruction = JMP_Op) then -- Operand fetch by default and PC + 1
				IROPwrite <= '1';
				RegRead <= '1';
				ALUSrcA <= "00";
				ALUSrcB <= "10";
				ALUOP <= "00";
				PCwrite <= '1';
				PCsrc <= "10";
			end if;
			
			state <= 2;
			next_s <= t3;
		when t3 => --Store to Reg A and B
		
			next_s <= t0;
			Zwrite <= '1';
			if(instruction = ADD_OP) then --Immediate AM
				ALUSrcA <= "01";
				ALUSrcB <= "01";
				ALUOP <= "00";
				RegWrite <= '1';
				MemToReg <= "01";
			elsif(instruction = ADD_Reg) then	--Register AM
				ALUSrcA <= "01";
				ALUSrcB <= "00";
				ALUOP <= "00";
				RegWrite <= '1';
				MemToReg <= "01";
			elsif(instruction = AND_OP) then --
				ALUSrcA <= "01";
				ALUSrcB <= "01";
				ALUOP <= "10";
				RegWrite <= '1';
				MemToReg <= "01";
			elsif(instruction = AND_Reg) then --
				ALUSrcA <= "01";
				ALUSrcB <= "00";
				ALUOP <= "10";
				RegWrite <= '1';
				MemToReg <= "01";
			elsif(instruction = SUB_OP) then --
				ALUSrcA <= "01";
				ALUSrcB <= "01";
				ALUOP <= "01";
			elsif(instruction = SUBV_OP) then --
				ALUSrcA <= "10";
				ALUSrcB <= "01";
				ALUOP <= "01";
				RegWrite <= '1';
				MemToReg <= "01";
			elsif(instruction = LDR_Reg) then --
				RegWrite <= '1';
				MemToReg <= "00";
			elsif(instruction = LDR_Op) then --
				RegWrite <= '1';
				MemToReg <= "00";
			elsif(instruction = LDR_OpAddress) then --This requires two more cycles
				MemRead <= '1';
				DataMemSrc <= "10";
				next_s <= t4;
			elsif(instruction = STR_Op) then
				DataMemWrSrc <= '0';
				DataMemSrc <= "01";
				MemWrite <= '1';
			elsif(instruction = STR_Reg) then
				DataMemWrSrc <= '1';
				DataMemSrc <= "01";
				MemWrite <= '1';
			elsif(instruction = STR_OpAddress) then
				DataMemWrSrc <= '1';
				DataMemSrc <= "10";
				MemWrite <= '1';
			elsif(instruction = SZ) then
				if(Z = '1') then
					PCwrite <= '1';
					PCsrc <= "00";
				end if;
			elsif(instruction = SSOP) then
				SSOPwrite <= '1';
			elsif(instruction = JMP_Op) then
				PCwrite <= '1';
				PCsrc <= "00";
			elsif(instruction = JMP_Reg) then
				PCwrite <= '1';
				PCsrc <= "01";
			end if;
			
			state <= 3;
		when t4 =>
			next_s <= t0;
			if(instruction = LDR_OpAddress) then
				MemToReg <= "10";
				RegWrite <= '1';
			end if;			
  end case;
end process;

end arch;
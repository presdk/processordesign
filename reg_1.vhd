library ieee;
use ieee.std_logic_1164.all;

entity reg_1 is
port(clk : in std_logic;
		d: in std_logic;
		ld : in std_logic;
		q : out std_logic
);
end reg_1;

architecture arch of reg_1 is
begin
	process(clk)
	begin
		if (clk'event and clk = '1') then
			if(ld = '1') then
				q <= d;
			end if;
		end if;
	end process;
end arch;





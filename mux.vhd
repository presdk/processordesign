library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std_unsigned;

package multiplexer_pkg is
	type bus_array is array(natural range <>) of std_logic_vector;
end package;

library ieee;
use ieee.std_logic_1164.all;
use work.multiplexer_pkg.all;
use ieee.numeric_std_unsigned.all;

entity mux is
	generic (INPUT_WIDTH : real;
				INPUT_SIZE : real;
				SEL_WIDTH : real
	);
	port(sel : in std_logic_vector(SEL_WIDTH-1 downto 0);
			d : in bus_array(integer range 2**SEL_WIDTH-1 downto 0)(INPUT_WIDTH-1 downto 0);
			q : out std_logic_vector(INPUT_WIDTH-1 downto 0)
	);
end mux;

architecture arch of mux is
begin
	q <= d(to_integer(unsigned(sel)));
end arch;
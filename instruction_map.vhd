library ieee;
use ieee.std_logic_1164.all;

package instruction_map is
	-- Concat OPCODE & AM to get below constant values
	constant ADD_Op : std_logic_vector(7 downto 0) := "111000" & "01";
	constant ADD_Reg : std_logic_vector(7 downto 0) := "111000" & "11";

	constant AND_Op : std_logic_vector(7 downto 0) := "001000" & "01";
	constant AND_Reg : std_logic_vector(7 downto 0) := "001000" & "11";

	constant SUB_Op : std_logic_vector(7 downto 0) := "000100" & "01";

	constant SUBV_Op : std_logic_vector(7 downto 0) := "000011" & "01";

	constant LDR_Op : std_logic_vector(7 downto 0) := "000000" & "01";
	constant LDR_Reg : std_logic_vector(7 downto 0) := "000000" & "11";
	constant LDR_OpAddress : std_logic_vector(7 downto 0) := "000000" & "01";

	constant STR_Op : std_logic_vector(7 downto 0) := "000010" & "01";
	constant STR_Reg : std_logic_vector(7 downto 0) := "000010" & "11";
	constant STR_OpAddress : std_logic_vector(7 downto 0) := "000010" & "01";

	constant SZ : std_logic_vector(7 downto 0) := "010100" & "01";

	constant SSOP : std_logic_vector(7 downto 0) := "111010" & "11";

	constant JMP_Op : std_logic_vector(7 downto 0) := "011000" & "01";
	constant JMP_Reg : std_logic_vector(7 downto 0) := "011000" & "11";
end instruction_map;
library ieee;
use ieee.std_logic_1164.all;

entity reg_16 is
port(clk, ld, clr : in std_logic;
		d: in std_logic_vector(15 downto 0);
		q : out std_logic_vector(15 downto 0)
);
end reg_16;

architecture arch of reg_16 is
begin
	process(clk, clr, ld)
	begin
		if (clr = '1') then
			q <= (others => '0');
		elsif (clk'event and clk = '1') then
			if (ld = '1') then
				q <= d;
			end if;
		end if;
	end process;
end arch;





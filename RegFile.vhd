library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity RegFile is
  port(
    outA        : out std_logic_vector(15 downto 0);
    outB        : out std_logic_vector(15 downto 0);
    input       : in  std_logic_vector(15 downto 0);
    writeEnable : in  std_logic;
	 readEnable	 : in  std_logic;
    regASel     : in  std_logic_vector(3 downto 0);
    regBSel     : in  std_logic_vector(3 downto 0);
    writeRegSel : in  std_logic_vector(3 downto 0);
    clk         : in  std_logic
    );
end RegFile;


architecture arch of RegFile is
  type registerFile is array(0 to 15) of std_logic_vector(15 downto 0);
  signal registers : registerFile;
begin
  regFile : process (clk) is
  begin
    if (clk'event and clk = '1') then
      -- Read A and B before bypass
		if readEnable = '1' then
			outA <= registers(to_integer(unsigned(regASel)));
			outB <= registers(to_integer(unsigned(regBSel)));
		end if;
      -- Write and bypass
      if writeEnable = '1' then
        registers(to_integer(unsigned(writeRegSel))) <= input;  -- Write
        --if regASel = writeRegSel then  -- Bypass for read A
          --outA <= input;
        --end if;
        --if regBSel = writeRegSel then  -- Bypass for read B
          --outB <= input;
        --end if;
      end if;
    end if;
  end process;
end arch;
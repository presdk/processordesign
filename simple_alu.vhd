library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
 
entity simple_alu is
port(   Reset : in std_logic; --clock signal
        A,B : in std_logic_vector(15 downto 0); --input operands
        Op : in std_logic_vector(1 downto 0); --Operation to be performed
        C : out std_logic_vector(15 downto 0);  --output of ALU
        Z : out std_logic
	);
end simple_alu;

architecture Behavioral of simple_alu is

--temporary signal declaration.
signal Reg1,Reg2,Reg3 : std_logic_vector(15 downto 0) := (others => '0');

begin

	Reg1 <= A;
	Reg2 <= B;
	



	Reg3 <= std_logic_vector(unsigned(Reg1) + unsigned(Reg2)) when Op = "00" else  --addition
			std_logic_vector(unsigned(Reg1) - unsigned(Reg2)) when Op = "01" else --subtraction
			Reg1 AND Reg2 when Op = "10" else -- AND gate
			Reg1 OR Reg2 when Op = "11" else --OR gate
			(others => '0');
			
		Z <= '1' when Reg3 = "0000000000000000" else '0';
	
	C <= Reg3;
end Behavioral;
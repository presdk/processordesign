library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;
 
entity adder_16 is
port(   A : in std_logic_vector(15 downto 0); --input operands
        C : out std_logic_vector(15 downto 0)  --output of ALU
	);
end adder_16;

architecture Behavioral of adder_16 is

--temporary signal declaration.
signal logical_one : std_logic_vector(15 downto 0) := "0000000000000001";

begin
	C <= std_logic_vector(unsigned(A) + unsigned(logical_one));
	
end Behavioral;
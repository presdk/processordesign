-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "04/08/2016 17:54:34"
                                                            
-- Vhdl Test Bench template for design  :  FetchBlock
-- 
-- Simulation tool : ModelSim (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY FetchBlock_vhd_tst IS
END FetchBlock_vhd_tst;
ARCHITECTURE FetchBlock_arch OF FetchBlock_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL A : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL ALUC : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL ALUOP : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL ALUSrcA : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL ALUSrcB : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL B : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL clk : STD_LOGIC;
SIGNAL DataMemSrc : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL DataMemWrSrc : STD_LOGIC;
SIGNAL DEBUG_current_instruction : STD_LOGIC_VECTOR(7 DOWNTO 0);
SIGNAL DEBUG_current_state : STD_LOGIC_VECTOR(31 DOWNTO 0);
SIGNAL IRMSB : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL IROPwrite : STD_LOGIC;
SIGNAL IRwrite : STD_LOGIC;
SIGNAL logical_one : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL MDROUT : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL MDRsrc : STD_LOGIC;
SIGNAL MDRwrite : STD_LOGIC;
SIGNAL MemRead : STD_LOGIC;
SIGNAL MemToReg : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL MemWrite : STD_LOGIC;
SIGNAL OPERAND : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL PCOUT : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL PCsrc : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL PCwrite : STD_LOGIC;
SIGNAL RegRead : STD_LOGIC;
SIGNAL RegWrite : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL SSOP : STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL SSOPwrite : STD_LOGIC;
SIGNAL start : STD_LOGIC;
SIGNAL Zwrite : STD_LOGIC;
COMPONENT FetchBlock
	PORT (
	A : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	ALUC : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	ALUOP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	ALUSrcA : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	ALUSrcB : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	B : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	clk : IN STD_LOGIC;
	DataMemSrc : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	DataMemWrSrc : OUT STD_LOGIC;
	DEBUG_current_instruction : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	DEBUG_current_state : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	IRMSB : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	IROPwrite : OUT STD_LOGIC;
	IRwrite : OUT STD_LOGIC;
	logical_one : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	MDROUT : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	MDRsrc : OUT STD_LOGIC;
	MDRwrite : OUT STD_LOGIC;
	MemRead : OUT STD_LOGIC;
	MemToReg : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	MemWrite : OUT STD_LOGIC;
	OPERAND : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	PCOUT : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	PCsrc : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	PCwrite : OUT STD_LOGIC;
	RegRead : OUT STD_LOGIC;
	RegWrite : OUT STD_LOGIC;
	reset : IN STD_LOGIC;
	SSOP : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	SSOPwrite : OUT STD_LOGIC;
	start : OUT STD_LOGIC;
	Zwrite : OUT STD_LOGIC
	);
END COMPONENT;
BEGIN
	i1 : FetchBlock
	PORT MAP (
-- list connections between master ports and signals
	A => A,
	ALUC => ALUC,
	ALUOP => ALUOP,
	ALUSrcA => ALUSrcA,
	ALUSrcB => ALUSrcB,
	B => B,
	clk => clk,
	DataMemSrc => DataMemSrc,
	DataMemWrSrc => DataMemWrSrc,
	DEBUG_current_instruction => DEBUG_current_instruction,
	DEBUG_current_state => DEBUG_current_state,
	IRMSB => IRMSB,
	IROPwrite => IROPwrite,
	IRwrite => IRwrite,
	logical_one => logical_one,
	MDROUT => MDROUT,
	MDRsrc => MDRsrc,
	MDRwrite => MDRwrite,
	MemRead => MemRead,
	MemToReg => MemToReg,
	MemWrite => MemWrite,
	OPERAND => OPERAND,
	PCOUT => PCOUT,
	PCsrc => PCsrc,
	PCwrite => PCwrite,
	RegRead => RegRead,
	RegWrite => RegWrite,
	reset => reset,
	SSOP => SSOP,
	SSOPwrite => SSOPwrite,
	start => start,
	Zwrite => Zwrite
	);
	reset <= '1', '0' after 5 ns;
clk_gen : process
begin
  loop
    clk <= '1'; wait for 10 ns;
    clk <= '0'; wait for 10 ns;
  end loop;
end process clk_gen;
	
init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          
END FetchBlock_arch;

-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- PROGRAM		"Quartus II 32-bit"
-- VERSION		"Version 13.0.0 Build 156 04/24/2013 SJ Full Version"
-- CREATED		"Sat Mar 26 15:19:16 2016"

LIBRARY ieee;
USE ieee.std_logic_1164.all; 

LIBRARY work;

ENTITY FetchBlock IS 
	PORT
	(
		clk :  IN  STD_LOGIC;
		IRwrite :  IN  STD_LOGIC;
		IROPwrite :  IN  STD_LOGIC;
		PCwrite :  IN  STD_LOGIC;
		PCIN :  IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
		IRMSB :  OUT  STD_LOGIC_VECTOR(15 DOWNTO 0);
		OPERAND :  OUT  STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END FetchBlock;

ARCHITECTURE bdf_type OF FetchBlock IS 

COMPONENT reg_16
	PORT(clk : IN STD_LOGIC;
		 ld : IN STD_LOGIC;
		 clr : IN STD_LOGIC;
		 d : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 q : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END COMPONENT;

COMPONENT programmemory
	PORT(address : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 readData : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
	);
END COMPONENT;

SIGNAL	SYNTHESIZED_WIRE_3 :  STD_LOGIC_VECTOR(15 DOWNTO 0);
SIGNAL	SYNTHESIZED_WIRE_2 :  STD_LOGIC_VECTOR(15 DOWNTO 0);


BEGIN 



b2v_IR : reg_16
PORT MAP(clk => clk,
		 ld => IRwrite,
		 d => SYNTHESIZED_WIRE_3,
		 q => IRMSB);


b2v_IROP : reg_16
PORT MAP(clk => clk,
		 ld => IROPwrite,
		 d => SYNTHESIZED_WIRE_3,
		 q => OPERAND);


b2v_PC : reg_16
PORT MAP(clk => clk,
		 ld => PCwrite,
		 d => PCIN,
		 q => SYNTHESIZED_WIRE_2);


b2v_ProgramMemory : programmemory
PORT MAP(address => SYNTHESIZED_WIRE_2,
		 readData => SYNTHESIZED_WIRE_3);


END bdf_type;